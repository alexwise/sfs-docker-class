# SFS Docker Class

### Prerequisites

Create an account at [Docker Hub](https://hub.docker.com/)

### Presentation

An up-to-date version of this class should be available [here](https://aayore.gitlab.io/sfs-docker-class/).

To view this presentation locally:

```
docker build -t presentation .
docker run -d -p 8080:80 presentation
```
Then open [http://localhost](http://localhost)
