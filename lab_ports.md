> From: ettiejr@ettiepy.com<br>
> Subject: A Simple Webserver
>
> Happy Tuesday!
>
>   We run a lot of webapp microservices with Docker.  One of the things you'll need to know is how to expose ports on a running container.  Set up a simple web server with Docker.  Don't worry about providing any custom content yet.
>
> -EJ

---

1. Use `docker search` to find the name of the "Apache HTTP Server" image.
2. Use the `--detach` with `docker run` (and the image you found in step 1) to launch a web server.
3. Run `docker ps` to see the running container.
4. Does a `curl localhost` work? (Note: In a web shell you won't be able to curl the normal http ports)
5. Try another `docker run` with the `--detach` and `--publish 8080:80` options.
6. What does `docker ps` show now?
7. Does a `curl localhost:8080` work?

Bonus:
- Why do we need the `--detach` option?
- What is the difference between `--publish` and `--publish-all`?