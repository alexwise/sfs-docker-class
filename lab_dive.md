> From: ettiejr@ettiepy.com<br>
> Subject: Docker Images Layers
>
>   I remember you said Docker Images are built out of "layers". Now that you have some new custom images, can you tell me what that's all about? I heard the "dive" tool is great for looking at a container image.
>
> Good luck playing NetHack,
>
> EJ

---

1. Ettie wants us to use the [Dive](https://github.com/wagoodman/dive) tool, so let's install that. `wget https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.deb && sudo apt install ./dive_0.9.2_linux_amd64.deb`
2. Let's take a look at our nethack image using Dive. `$ dive mynethack`
3. Use the arrow keys to navigate. Press Tab to switch between the Layers column and the Filesystem column
4. Which layers made the most changes? 
5. Press Ctrl+C to exit

---

Bonus:

- Take a look at some other images using Dive!
- Why might you want to run certain operations later in your Dockerfile?