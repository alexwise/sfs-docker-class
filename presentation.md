# Introduction to Docker

!

# $ whoami
![whoami](images/whoami.png)

!

# Mental Models 

!

# "The Diagram"

![The Diagram](images/virtualization-vs-containers.png)


Source: RedHat.com

!


![Linux PoC](images/LinuxPoC.png)

!

```
 "Containers are processes, 
     born from tarballs, 
 anchored to namespaces, 
     controlled by cgroups"
```
### Alice Goldfuss

!

### Several Low-Level Linux Features "Stapled Together"
- Control Groups (CGroups)
- Namespaces
- Union Filesystems

!

# Introduction

- What is Docker?
- Why use Docker?
- Docker and 12-Factor design

<!-- No persistent storage! -->

!

## Installation

- We'll be doing this ~*~*IN THE CLOUD*~*~
- Log into your Google Cloud account and open up a web shell
- Docker is pre-installed, pre-configured, and we can avoid WOMM issues

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_installation.md)

<!-- Lab: lab_installation.md -->

!

## Running a Container

- `docker run <arguments for run commands> <name of image> <commands to run inside image>`
- `docker help run`
- Get stuff from [Docker Hub](https://hub.docker.com/)
- `docker search`
- Be careful!

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_running.md)

<!-- Lab: lab_running.md -->
<!-- Discuss interactive vs. detached -->

!

## What's a cgroup?

- A Cgroup limits what resources a process (container) has access to
  - CPU
  - Memory
  - Devices
  - Number of PIDs
  - etc.

`tree /sys/fs/cgroup`


[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_cgroups.md)

<!-- Lab: lab_cgroups.md -->

!

## Exposing Ports

- Containers are inaccessible by default

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_ports.md)

<!-- Lab: lab_ports.md -->

!

## What's a Namespace?

- A namespace limits what a process can share resources with
  - Network
  - Filesystem Mounts
  - Processes
  - UIDs/Group IDs
  - etc.

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_netns.md)


<!-- docker run uses separate network namespace per container -->
<!-- Lab: lab_netns.md -->

!


## Terms

- Registry vs. Repo
- Image vs. Container
- Tags
- Push and Pull

<!-- No lab. -->

!

## Passing Config

- Environment variables
- Environment files

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_configs.md)

<!-- Lab: lab_configs.md -->

!

## Stateful Data (or Configs)

- Don't count on container data
- Mounting volumes

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_state.md)

<!-- Lab: lab_state.md -->

!

## Stopping

- `docker stop`
- `docker kill`
- `docker rm --force`

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_stop.md)

!

## Cleaning Up

- `docker ps --all`
- `docker images`
- `docker {object} prune` : container, image, system
- `docker rm` and `docker rmi`

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_cleanup.md)

<!-- Lab: Examine what we have running, clean it up -->

!

## Building Your Own

- Dockerfile
- Context
- `docker build <context>`

[Lab 1](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_nethack.md)

[Lab 2](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_webserver.md)

<!-- Lab: Create our own Dockerfile, build it -->

!

## Layers, Tarballs, and Union Filesystems?

```
Step 1/4 : FROM ubuntu:20.04
20.04: Pulling from library/ubuntu
6a5697faee43: Pull complete
ba13d3bc422b: Pull complete
a254829d9e55: Pull complete
Digest: sha256:fff16eea1a8ae92867721d90c59a75652ea66d29c05294e6e2f898704bdb8cf1
Status: Downloaded newer image for ubuntu:20.04
```

!

## Pushing to DockerHub

- Sharing with others
- Facilitating deployments
- Moving an image

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_push.md)

<!-- Lab: Push our image -->

!

## Artifacts

- Commit hash
- Software package
- Container

<!-- Lab: ??? -->

!

## Resources

- Alice Goldfuss, The Container Operator's Manual:  [YouTube](https://www.youtube.com/watch?reload=9&v=sJx_emIiABk)
- Samuel Karp, Linux Container Primitives: cgroups, namespaces, and more!: [YouTube](https://www.youtube.com/watch?v=x1npPrzyKfs&feature=emb_logo)
- Original Course by Aaron Brown, @aayore. 

!

## The End

- Thank you!

## Questions?

- What should I put in the next Docker class?
