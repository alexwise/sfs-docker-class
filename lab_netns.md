> From: ettiejr@ettiepy.com<br>
> Subject: How Does Container Networking Work?!
>
> Hey There!
>
>   Thanks for exposing that webserver. I think I understand how cgroups limit what resources a container can use, but how do namespaces limit what a container can talk to?! 
>
> -EJ

---


1. Let's start by running a basic container so we can examine the networking. `$ docker run  --name redis --detach redis`
2. And let's grab the Process ID `$ pidof redis-server`
3. `readlink` is a native linux tool that can tell us what namespaces as Process is a member of. Since we care about the Networking namespace, let's check that. `$ sudo readlink /proc/$PID/ns/net`
4. We can use `docker inspect` to give us networking information about the container. `$ docker inspect redis | jq .[0].NetworkSettings.IPAddress`
5. The `nsenter` command lets us "go into" a namespace and run commands there. Let's see if the IP of the eth0 device in there matches docker `$ sudo nsenter --target $PID --net ifconfig eth0`

---

1. Docker creates a "veth" device on the host to bridge across networking namespaces. Let's look at it. `$ ip link | grep veth`. `$ ifconfig $VETH_ID`