> From: ettiejr@ettiepy.com<br>
> Subject: Running Containers
>
> Good afternoon,
>
>   Since you've gotten Docker installed, it's time to try and run something.  Since pretty much everything starts with a Hello World app, go ahead and give it a try with a local Docker instance.
>
> Good luck,
>
> EJ

---

1. Try `docker help run` at the command line.
2. Use `docker search` to find an ubuntu image.
3. Run `docker run --tty ubuntu echo "i am container."`
4. Use `docker run` (with other stuff) to spit out `Hello World`

Bonus:
- What other commands can you run besides echo on your ubuntu container? `docker run --tty ubuntu yes`?
