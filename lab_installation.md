You've just started work at a new company, Aunt Ettie's House of Py.  You've got a good handle on the rest of your job, but they're a Docker shop and you've never used Docker before.  They knew that when they hired you, and they've allocated some ramp-up time for you to get familiar with Docker before jumping in feet first.

---

> From: ettiejr@ettiepy.com<br>
> Subject: Getting Started With Docker
>
> Hey, you,
>
>   Welcome!  I hope your first days is going well so far.  As discussed previously, you'll need to start learning Docker!  For today, head on over to cloud.google.com and load up a web shell.
>
> Let me know if you have any trouble.
>
> -EJ

---

1. Head over to [cloud.google.com](https://console.cloud.google.com/) and get logged in.
2. Click on the "Activate Cloud Shell" button in the upper right corner.
3. Create a new directory to work in. `$ mkdir -p sfs-docker-class`
4. Check that docker is installed. `$ docker version`